let images = document.querySelectorAll(".work_list-images-item");
let workFilterButtons = document.querySelectorAll(".work_list-item");
let loadMore = document.querySelector(".load_more_button");
let loadingButton = document.querySelector(".load_more_button_loading")


workFilterButtons.forEach(function(workFilter){
    workFilter.addEventListener("click", function(){

        for(let i = 0; i < workFilterButtons.length; i++){
            if(workFilterButtons[i].classList.contains("active_work_list")){
                workFilterButtons[i].classList.remove("active_work_list");
            }
        }

        workFilter.classList.add("active_work_list");

        images.forEach(function(image){
            image.style.display = "none";
        })

        if(workFilter.dataset.gallery === "All"){
            images.forEach(function(image){
                image.style.display = "block";
            })
        }

        images.forEach(function(image){
            if(image.dataset.gallery === workFilter.dataset.gallery){
                image.style.display = "block";
            }
        })

    })
})

function showNewImages(){

    images.forEach(function(image){
        if(image.classList.contains("not_loaded")){
            image.classList.remove("not_loaded");
        }
    })

    loadingButton.style.display = "none";
}

loadMore.addEventListener("click", function(){
    loadMore.style.display = "none";
    loadingButton.style.display = "block";
    let loading = setTimeout(showNewImages, 2000);
})