let services = document.querySelectorAll(".services-list-item");
let serviceInfo = document.querySelectorAll(".service_info");

services.forEach(function(service){
    service.addEventListener("click", function(){
        for(let i = 0; i < services.length; i++){
            if(services[i].classList.contains("active_service")){
                services[i].classList.remove("active_service");
            }
        }

        for(let i = 0; i <serviceInfo.length; i++){
            serviceInfo[i].classList.remove("active_service-info");
        }

        service.classList.add("active_service");

        let activeService = document.querySelector(".active_service");

        serviceInfo.forEach(function(info){
            if(info.dataset.service === activeService.dataset.service){
                info.classList.add("active_service-info");
            }
        })

       


        })

        
        
})
