let users = document.querySelectorAll(".user_selection-list-item");
let usersInfo = document.querySelectorAll(".reviews-detailed_info");
let backArrow = document.querySelector(".reviews-user_selection-backarrow");
let forwardsArrow = document.querySelector(".reviews-user_selection-forwardsarrow");
let userIndex = 1;

users.forEach(function(user){
    user.addEventListener("click", function(){

        users.forEach(function(userIter){
            if(userIter.classList.contains("active_user")){
                userIter.classList.remove("active_user")
            }

            if(user.dataset.person === userIter.dataset.person){
                userIter.classList.add("active_user");
                userIndex = parseInt(userIter.dataset.person);
            }
            
        })


        usersInfo.forEach(function(info){
            if(info.classList.contains("active_info")){
                info.classList.remove("active_info")
            }

            if(user.dataset.person === info.dataset.person){
                info.classList.add("active_info");
            }
        })
    })
})

backArrow.addEventListener("click", function(){
    if(userIndex > 1){
        userIndex = userIndex - 1;


        users.forEach(function(userIter){
            if(userIter.classList.contains("active_user")){
                userIter.classList.remove("active_user")
            }

            if(userIndex === parseInt(userIter.dataset.person)){
                userIter.classList.add("active_user");
                userIndex = parseInt(userIter.dataset.person);
                console.log(userIndex)
            }
            
        })


        usersInfo.forEach(function(info){
            if(info.classList.contains("active_info")){
                info.classList.remove("active_info")
            }

            if(userIndex === parseInt(info.dataset.person)){
                info.classList.add("active_info");
            }
        })

    }
})

forwardsArrow.addEventListener("click", function(){
    if(userIndex < 4){
        userIndex = userIndex + 1;


        users.forEach(function(userIter){
            if(userIter.classList.contains("active_user")){
                userIter.classList.remove("active_user")
            }

            if(userIndex === parseInt(userIter.dataset.person)){
                userIter.classList.add("active_user");
                userIndex = parseInt(userIter.dataset.person);
                console.log(userIndex)
            }
            
        })


        usersInfo.forEach(function(info){
            if(info.classList.contains("active_info")){
                info.classList.remove("active_info")
            }

            if(userIndex === parseInt(info.dataset.person)){
                info.classList.add("active_info");
            }
        })

    }
})